#!/bin/bash

echo 'N,D,work_mem,hashagg,sort,HashAggregate,GroupAggregate,workers,memusage,batches,disk' > results.csv
rm -f explains.log

for n in 100000 1000000 10000000 100000000; do

	for d in 1000 10000 100000 1000000 10000000 100000000; do

		if [ "$d" -gt "$n" ]; then
			continue
		fi

		psql test <<EOF
DROP TABLE IF EXISTS t;
CREATE TABLE t AS SELECT ($d * random())::int AS a from generate_series(1,$n) s(i);
ANALYZE t;
CHECKPOINT;
EOF

		for hash in on off; do

			for group in on off; do

				for workers in 0 8; do

					for wm in 4MB 16MB 64MB 128MB; do

						times=""

psql test > tmp.log 2>&1 <<EOF
SET work_mem = '$wm';
SET enable_hashagg = '$hash';
SET enable_sort = '$group';
SET max_parallel_workers_per_gather = $workers;

EXPLAIN ANALYZE SELECT * FROM (SELECT a, count(*) FROM t GROUP BY a OFFSET 1000000000) foo;
EOF

						echo "===================================================================================" >> explains.log
						echo "n=$n d=$d enable_hashagg=$hash enable_sort=$group work_mem=$wm workers=$workers" >> explains.log
						cat tmp.log >> explains.log
						echo "===================================================================================" >> explains.log

						memusage=`grep Batches tmp.log | awk '{print $3}'`
						nbatches=`grep Batches tmp.log | awk '{print $5}'`
						disk=`grep Batches tmp.log | awk '{print $7}'`
						nhash=`grep HashAggregate tmp.log | wc -l`
						ngroup=`grep GroupAggregate tmp.log | wc -l`

						for r in `seq 1 1`; do

							psql test > tmp.log 2>&1 <<EOF
SET work_mem = '$wm';
SET enable_hashagg = '$hash';
SET enable_sort = '$group';
SET max_parallel_workers_per_gather = $workers;
SET min_parallel_index_scan_size = '64kB';
SET min_parallel_table_scan_size = '64kB';

\timing on
SELECT * FROM (SELECT a, count(*) FROM t GROUP BY a OFFSET 1000000000) foo;
EOF

							t=`grep 'Time:' tmp.log | awk '{print $2}'`
							times="$times,$t"

						done

						echo $n,$d,$wm,$hash,$group,$nhash,$ngroup,$workers,$memusage,$nbatches,$disk$times >> results.csv

					done

				done

			done

		done

	done

done
